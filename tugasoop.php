<?php

// abstract agar tidak bisa di intansiasi
abstract class Hewan  {
    // set ke public agar tidak perlu menggunakan setter & getter
    public  $nama,
            $darah = 50,
            $jumlahKaki,
            $keahlian;

    protected function atraksi() {
        return $this->nama. ' sedang ' .$this->keahlian;
    }
}

// abstract agar tidak bisa di intansiasi
// extend ke hewan agar bisa menggunakan property & method dari class hewan
abstract class Fight extends Hewan {
    // set ke public agar tidak perlu menggunakan setter & getter
    public  $attackPower,
            $defencePower;

    protected function serang($target){
        return $target.' sedang menyerang '.$this->nama;
    }

    protected function diserang($target){
        // efek diserang : darah sekarang – attackPower penyerang / defencePower yang diserang
        $darah_sekarang = $this->darah;
        $this->darah = $darah_sekarang - $target['attack'] / $this->defencePower;
        return "{$this->nama} sedang diserang!<br/>sisa darah {$this->nama} : {$this->darah} ({$darah_sekarang} - {$target['attack']} / {$this->defencePower})";
    }
}

class Elang extends Fight{
    public function __construct($jumlahKaki = 2, $keahlian = 'terbang tinggi', $attackPower = 10, $defencePower = 5){
        // jumlahKaki bernilai 2, dan keahlian bernilai “terbang tinggi”, attackPower = 10 , deffencePower = 5
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;

    }

    public function getInfoHewan($target=array(),$nama=null) {
        // set defaul name jika tidak dikasih nama
        if($nama == null) {
            $this->nama = 'Elang';
        } else {
            $this->nama = $nama;
        }
        $string = "jenis_hewan : Harimau<br/>
        nama : {$this->nama}<br/>
        darah : {$this->darah}<br/>
        jumlahKaki : {$this->jumlahKaki}<br/>
        keahlian : {$this->keahlian}<br/>
        attackPower : {$this->attackPower}<br/>
        defencePower : {$this->defencePower}<hr/>
        atraksi : {$this->atraksi()}<hr/>
        <strong>Fight</strong><br/>
        serang : {$this->serang($target['nama'])}<br/>
        diserang : {$this->diserang($target)}";
        return  $string;
    }
}

class Harimau extends Fight {

    public function __construct($jumlahKaki = 4, $keahlian = 'lari cepat', $attackPower = 7, $defencePower = 8){
        // jumlahKaki bernilai 4, dan keahlian bernilai “lari cepat” , attackPower = 7 , deffencePower = 8
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;

    }

    public function getInfoHewan($target=array(),$nama=null) {
        // set defaul name jika tidak dikasih nama
        if($nama == null) {
            $this->nama = 'Harimau';
        } else {
            $this->nama = $nama;
        }
        $string = "jenis_hewan : Harimau<br/>
        nama : {$this->nama}<br/>
        darah : {$this->darah}<br/>
        jumlahKaki : {$this->jumlahKaki}<br/>
        keahlian : {$this->keahlian}<br/>
        attackPower : {$this->attackPower}<br/>
        defencePower : {$this->defencePower}<hr/>
        atraksi : {$this->atraksi()}<hr/>
        <strong>Fight</strong><br/>
        serang : {$this->serang($target['nama'])}<br/>
        diserang : {$this->diserang($target)}";
        return  $string;
    }

}

$e = new Elang;
$e->nama = 'Elang';
$elang = $e->nama;
$elang_attack = $e->attackPower;
$elang_defence = $e->defencePower;

$h = new Harimau;
$h->nama = 'Harimau';
$harimau = $h->nama;
$harimau_attack = $h->attackPower;
$harimau_defence = $h->defencePower;

$harimau_data = array(
    'nama'=>$harimau,
    'attack'=>$harimau_attack,
    'defence'=>$harimau_defence
);

$elang_data = array(
    'nama'=>$elang,
    'attack'=>$elang_attack,
    'defence'=>$elang_defence
);

$elang_serang = $elang_data;
echo $h->getInfoHewan($elang_serang, $h->nama);

$harimau_serang = $harimau_data;
echo $e->getInfoHewan($harimau_serang, $e->nama);
